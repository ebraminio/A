# asdasd sadas sadas da sd as d (LTR)
# سیشسی شسی شس ی شسی سش (RTL)

asdasd asd as d as da sd asdas das d asd (LTR)

شسیش سی شس یشس ی شس یش سی شسی (RTL)

LTR:
> asdasdasdasdasDasd as d as da sd

RTL:
> سشیشس یشس ی شس یش سی شس ی شسی

LTR: (don't mess with codes even if they contain some RTL text)
''' (replace these with three backtick on your test)
سشیسی
شسیشسی
'''

LTR:
1. asdasds
2. asdasdasd


RTL
1. شسیشسی
2. ضسشیشسیشسی

LTR:
* ABDCVFDS
- ASDASDASd
+ ASDASDASd

RTL:
* سشیشسیشسی
- سشیشسی شسیش سی شس ی
+ شسیشسی شسی شس ی سشی


* asdas
  * asdasd
  * asdasd


سشیشسی

* شس
  * شسیشس
  * شسیشسی
